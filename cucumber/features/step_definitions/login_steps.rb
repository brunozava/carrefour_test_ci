Given("Access carrefour page") do
    idPage.load
end
  
Given("I have e-mail {string} and pass {string}") do |user, senha|
    @user = user
    @senha = senha
end
  
When("I do login in submarino website") do
    idPage.do_login(@user,@senha)
end
  
Then("I should see the message {string}") do |message|
    expect(nav.welcome_message.text).to have_content message
end

Given("I have the following credentials") do |table|
    @users = table.hashes
end
  
When("I try to login") do
    @users.each do |user|
        idPage.clean_fields
        idPage.do_login(user['email'], user['password'])
    end
end

Then("{string} message should be displayed in login page") do |message|
    expect(nav.login_error_message.text).to have_content message
end