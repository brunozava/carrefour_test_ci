Feature: Login using existing credentials
    As a user I want to login in submarino eStore

    Background:
        * Access carrefour page

    Scenario: User login using existing credentials
        Given I have e-mail "testezava@hotmail.com" and pass "130887"
        When I do login in submarino website
        Then I should see the message "Bruno"
    
    @wip
    Scenario: Login with wrong credentials
        Given I have the following credentials
            |email|password|message|
            |aaa@tfhf.com|1222|E-mail ou senha incorretos|
            |testezava@hotmail.com|1222|E-mail ou senha incorretos|
         When I try to login
         Then "E-mail ou senha incorretos" message should be displayed in login page 