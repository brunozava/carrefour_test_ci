class NavPage <SitePrism::Page
    element :welcome_message, '.usr-nick'
    element :login_error_message, 'span[class="entrar-formError --zeroLeft"]'
end