class IdPage <SitePrism::Page
    set_url '/simple-login'
    element :existing_email, '#email-input'
    element :existing_pass, '#password-input'
    element :login_button, '#login-button'


    def do_login(email,pass)
        self.existing_email.set email
        self.existing_pass.set pass
        self.login_button.click
    end

    def clean_fields
        self.existing_email.set ''
        self.existing_pass.set ''
    end


end